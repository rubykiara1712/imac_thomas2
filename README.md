# IMAC_Thomas2

![alt text](https://www.zupimages.net/up/22/21/0yma.png)


**OPEN GL x PROG IMAC1**

> Debeaune Tristan  
> Gervais Wendy  
> Huet Quentin  



## Lancer le jeu

dans un terminal (à la racine de imac_thomas2) :
```
make jeu 
```

```
bin/jeu.out
```

si erreur, commencer par : 
```
make clean
```

puis répéter les deux commandes du dessus.

## Mode debug (en jeu)

Afficher le quadtree
```
B
```

Passer le niveau actuel
```
N
```

## Commandes (en jeu)

Se déplacer
```
Flèche gauche, flèche droite
```

Sauter
```
Espace
```

Changer de personnage
```
Tabulation
```

Menu pause
```
Echap
```

Recommencer le niveau
```
R
```


## Commandes (menu)

Se déplacer
```
Flèche haut, flèche bas
```

Valider
```
Enter
```



